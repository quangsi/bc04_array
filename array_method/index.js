var numArr = ["2", "4", "6"];
// hỗ trợ duyệt mảng ~ thường dùng để show thông tin
numArr.forEach(function (item, index) {
  console.log("item", item, index);
});

// duyệt mảng và hỗ trợ returb ~ tạo ra mảng mới ~ từ dữ liệu thô sang dữ liệu cần thiết
var resultArr = numArr.map(function (number) {
  console.log("number: ", number);

  return number * 10;
});
console.log("resultArr: ", resultArr);

// call back
function introduce(user, callback) {
  callback(user);
  console.log("Have a good day");
}

function sayHello(username) {
  console.log("Hello " + username);
}

function sayGoodbye(user) {
  console.log("Goodbye to " + user);
}
introduce("Alice", sayHello);
introduce("Alice", sayGoodbye);

// push pop

var menu = ["trà sữa", "cafe"];

menu.push("Trà đá");

var lastItem;
console.log("lanstItem: ", lastItem);
console.log("menu: ", menu);
menu.unshift("Milk tea");
console.log("menu: ", menu);
// mutable - immutable

// CRUD

// indexOf ~ tìm vị trí

var indexCafe = menu.indexOf("cafe");
console.log("indexCafe: ", indexCafe);

if (indexCafe !== -1) {
  console.log("Tìm thấy");
} else {
  console.log("Ko tìm thấy");
}

var numbers = [3, 6, 8, 9, 10];

var result3 = numbers.filter(function (item) {
  return item < 8;
});

console.log("result3: ", result3);
var result2 = numbers.slice(1, 3);
console.log("result2: ", result2);

var result1 = menu.splice(indexCafe, 1);
console.log("numbers: ", numbers);
console.log("result1: ", result1);
