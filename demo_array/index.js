// array,object ~ pass by reference

// number , string, boolean ~ pass by value

var numArr = [2, 4, 8, 6];
console.log("numArr: ", numArr);

var menu = ["Bún bò", "Bún real", "Hủ tíu", "Cháo lòng"];
// [2] : index ~ chỉ số. Vị trí của phần tử trong array
menu[2] = "Bánh canh";
console.log("menu: ", menu);

// length : độ dài array ~ số lượng phần tử
var soLuongMonAn = menu.length;
console.log("soLuongMonAn: ", soLuongMonAn);

// truy xuất phần tử cuối cùng
console.log(menu[menu.length - 1]);
// duyệt mảng
for (var index = 0; index < menu.length; index++) {
  var monAn = menu[index];
  console.log(monAn, index);
}
